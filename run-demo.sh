#!/bin/bash
set -e
SUDO_CMD="sudo"
if groups $(id -un) | grep -E " docker {0,1}" > /dev/null 2>&1; then
    SUDO_CMD=""
fi

DOCKER_TAG="zephyros-sim-demo"
${SUDO_CMD} docker build -t "${DOCKER_TAG}" .
DOCKER_CMD_FILE="$(mktemp)"
trap 'rm "${DOCKER_CMD_FILE}"' EXIT
cat << 'BASH' > "${DOCKER_CMD_FILE}"
west build -b nucleo_f103rb .

LEFT_CMD='bash --rcfile <(echo "west run")'
RIGHT_CMD='bash --rcfile <(echo "sleep 5 && west sim gpio")'
SHELL=/bin/bash tmux new "${LEFT_CMD}" \; splitw -h "${RIGHT_CMD}"
BASH
${SUDO_CMD} docker run -it --rm \
    --mount type=bind,src="${DOCKER_CMD_FILE}",dst="/bashrc" \
    "${DOCKER_TAG}" --rcfile /bashrc
