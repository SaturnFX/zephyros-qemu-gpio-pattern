#!/bin/bash
set -e
QEMU_MAJOR_VER=6
QEMU_MINIOR_VER=2
QEMU_BASE_URL="https://download.qemu.org/"

QEMU_HTML_PATTERN='<td[^>]*>[[:space:]]*'
QEMU_HTML_PATTERN+='<a href="'
QEMU_HTML_PATTERN+='(qemu-'
QEMU_HTML_PATTERN+="${QEMU_MAJOR_VER}"
QEMU_HTML_PATTERN+='\.'
QEMU_HTML_PATTERN+="${QEMU_MINIOR_VER}"
QEMU_HTML_PATTERN+='\.[[:digit:]]+\.tar\.[[:alnum:]]*)'
QEMU_HTML_PATTERN+='">[^<]+</a>[[:space:]]*'
QEMU_HTML_PATTERN+='</td>[[:space:]]*'
QEMU_HTML_PATTERN+='<td[^>]*>'
QEMU_HTML_PATTERN+='([[:digit:]]{4}-[[:digit:]]{2}-[[:digit:]]{2} '
QEMU_HTML_PATTERN+='[[:digit:]]{2}:[[:digit:]]{2})[[:space:]]*'
QEMU_HTML_PATTERN+='</td>'

wget -qO- "${QEMU_BASE_URL}" | tr -d '\n' \
    | sed 's|</tr>|</tr>\n|g' \
    | sed -rn 's|^.*'"${QEMU_HTML_PATTERN}"'.*$|\1;\2|p' \
    | while IFS= read -r QEMU_PACKAGE_LINE; do
        QEMU_FILE_NAME="${QEMU_PACKAGE_LINE%;*}"
        QEMU_REL_DATE="${QEMU_PACKAGE_LINE#*;}"
        echo -n "${QEMU_FILE_NAME};"
        echo "$(date -d "${QEMU_REL_DATE}" +%s)"
    done \
    | sort -t ";" -k 2rn \
    | (
        IFS= read -r -d ";" QEMU_LATEST_NAME
        echo "${QEMU_BASE_URL}${QEMU_LATEST_NAME}"
    )
