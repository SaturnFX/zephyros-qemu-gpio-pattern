/*
 * STM32F1xx GPIO registers device
 *
 * Copyright (c) 2022 Michał Prochera <saturniak8888@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "qemu/osdep.h"
#include "qemu/module.h"
#include "migration/vmstate.h"
#include "hw/gpio/stm32f1xx_gpio.h"

#define GPIO_CRL_OFFSET 0x00
#define GPIO_CRL_DEF_VAL 0x44444444

#define GPIO_CRH_OFFSET 0x04
#define GPIO_CRH_DEF_VAL 0x44444444

#define GPIO_IDR_OFFSET 0x08
#define GPIO_IDR_DEF_VAL 0x00000000

#define GPIO_ODR_OFFSET 0x0C
#define GPIO_ODR_DEF_VAL 0x00000000

#define GPIO_BSRR_OFFSET 0x10

#define GPIO_BRR_OFFSET 0x14

#define GPIO_16_BIT_SHIFT 16
#define GPIO_16_BIT_MSK 0xFFFF

static uint64_t stm32f1xx_gpio_read(void *opaque,
    hwaddr addr, unsigned size)
{
    STM32F1XXGPIOState *s = opaque;

    switch (addr) {
    case GPIO_CRL_OFFSET:
        return s->gpio_crl;
    case GPIO_CRH_OFFSET:
        return s->gpio_crh;
    case GPIO_IDR_OFFSET:
        return s->gpio_idr;
    case GPIO_ODR_OFFSET:
        return s->gpio_odr;
    }
    return 0;
}

static void stm32f1xx_gpio_write(void *opaque,
    hwaddr addr, uint64_t data, unsigned size)
{
    STM32F1XXGPIOState *s = opaque;
    uint32_t set_bits = 0, reset_bits = 0;

    switch (addr) {
    case GPIO_CRL_OFFSET:
        s->gpio_crl = data;
        break;
    case GPIO_CRH_OFFSET:
        s->gpio_crh = data;
        break;
    case GPIO_IDR_OFFSET:
        /* Normally read-only, used for simulation purposes */
        s->gpio_idr = data & GPIO_16_BIT_MSK;
        break;
    case GPIO_ODR_OFFSET:
        s->gpio_odr = data & GPIO_16_BIT_MSK;
        break;
    case GPIO_BSRR_OFFSET:
        /* 
            Lower 16-bits sets the GPIO pins 
        */
        set_bits = data & GPIO_16_BIT_MSK;
        /* 
            Higher 16-bits resets the GPIO pins 
            if those pins are not set at the same time
        */
        reset_bits = (data >> GPIO_16_BIT_SHIFT) & (~set_bits);
        s->gpio_odr = (s->gpio_odr | set_bits) & (~reset_bits);
        break;
    case GPIO_BRR_OFFSET:
        /* 
            Lower 16-bits resets the GPIO pins 
        */
        reset_bits = data & GPIO_16_BIT_MSK;
        s->gpio_odr = s->gpio_odr & (~reset_bits);
        break;
    }
}

static const MemoryRegionOps stm32f1xx_gpio_ops = {
    .read = stm32f1xx_gpio_read,
    .write = stm32f1xx_gpio_write,
    .endianness = DEVICE_NATIVE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4,
        .unaligned = false
    }
};

static void stm32f1xx_gpio_init(Object *obj)
{
    STM32F1XXGPIOState *s = STM32F1XX_GPIO(obj);

    memory_region_init_io(&s->iomem, obj,
        &stm32f1xx_gpio_ops, s,
        TYPE_STM32F1XX_GPIO, 0x400);
    sysbus_init_mmio(SYS_BUS_DEVICE(obj), &s->iomem);
}

static const VMStateDescription stm32f1xx_gpio_vmstate = {
    .name = TYPE_STM32F1XX_GPIO,
    .version_id = 1,
    .minimum_version_id = 1,
    .fields = (VMStateField[]) {
        VMSTATE_UINT32(gpio_crl, STM32F1XXGPIOState),
        VMSTATE_UINT32(gpio_crh, STM32F1XXGPIOState),
        VMSTATE_UINT32(gpio_idr, STM32F1XXGPIOState),
        VMSTATE_UINT32(gpio_odr, STM32F1XXGPIOState),
        VMSTATE_END_OF_LIST()
    }
};

static void stm32f1xx_gpio_reset_enter(Object *obj, ResetType type)
{
    STM32F1XXGPIOState *s = STM32F1XX_GPIO(obj);

    s->gpio_crl = GPIO_CRL_DEF_VAL;
    s->gpio_crh = GPIO_CRH_DEF_VAL;
    s->gpio_idr = GPIO_IDR_DEF_VAL;
    s->gpio_odr = GPIO_ODR_DEF_VAL;
}

static void stm32f1xx_gpio_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);
    ResettableClass *rc = RESETTABLE_CLASS(klass);

    dc->vmsd = &stm32f1xx_gpio_vmstate;
    rc->phases.enter = stm32f1xx_gpio_reset_enter;
}

static const TypeInfo stm32f1xx_gpio_info = {
    .name          = TYPE_STM32F1XX_GPIO,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(STM32F1XXGPIOState),
    .class_init    = stm32f1xx_gpio_class_init,
    .instance_init = stm32f1xx_gpio_init,
};

static void stm32f1xx_gpio_types(void)
{
    type_register_static(&stm32f1xx_gpio_info);
}

type_init(stm32f1xx_gpio_types);
