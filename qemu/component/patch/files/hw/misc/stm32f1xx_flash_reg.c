/*
 * STM32F1xx flash register device
 *
 * Copyright (c) 2022 Michał Prochera <saturniak8888@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "qemu/osdep.h"
#include "qemu/module.h"
#include "migration/vmstate.h"
#include "hw/misc/stm32f1xx_flash_reg.h"

#define FLASH_REG_ACR_OFFSET 0x0
#define FLASH_REG_ACR_DEF_VAL 0x00000030

static uint64_t stm32f1xx_flash_reg_read(void *opaque,
    hwaddr addr, unsigned size)
{
    STM32F1XXFlashRegState *s = opaque;

    switch (addr) {
    case FLASH_REG_ACR_OFFSET:
        return s->flash_acr;
    }
    return 0;
}

static void stm32f1xx_flash_reg_write(void *opaque,
    hwaddr addr, uint64_t data, unsigned size)
{
    STM32F1XXFlashRegState *s = opaque;

    switch (addr) {
    case FLASH_REG_ACR_OFFSET:
        s->flash_acr = data;
        break;
    }
}

static const MemoryRegionOps stm32f1xx_flash_reg_ops = {
    .read = stm32f1xx_flash_reg_read,
    .write = stm32f1xx_flash_reg_write,
    .endianness = DEVICE_NATIVE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4,
        .unaligned = false
    }
};

static void stm32f1xx_flash_reg_init(Object *obj)
{
    STM32F1XXFlashRegState *s = STM32F1XX_FLASH_REG(obj);

    memory_region_init_io(&s->iomem, obj,
        &stm32f1xx_flash_reg_ops, s,
        TYPE_STM32F1XX_FLASH_REG, 0x400);
    sysbus_init_mmio(SYS_BUS_DEVICE(obj), &s->iomem);
}

static const VMStateDescription stm32f1xx_flash_reg_vmstate = {
    .name = TYPE_STM32F1XX_FLASH_REG,
    .version_id = 1,
    .minimum_version_id = 1,
    .fields = (VMStateField[]) {
        VMSTATE_UINT32(flash_acr, STM32F1XXFlashRegState),
        VMSTATE_END_OF_LIST()
    }
};

static void stm32f1xx_flash_reg_reset_enter(Object *obj, ResetType type)
{
    STM32F1XXFlashRegState *s = STM32F1XX_FLASH_REG(obj);

    s->flash_acr = FLASH_REG_ACR_DEF_VAL;
}

static void stm32f1xx_flash_reg_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);
    ResettableClass *rc = RESETTABLE_CLASS(klass);

    dc->vmsd = &stm32f1xx_flash_reg_vmstate;
    rc->phases.enter = stm32f1xx_flash_reg_reset_enter;
}

static const TypeInfo stm32f1xx_flash_reg_info = {
    .name          = TYPE_STM32F1XX_FLASH_REG,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(STM32F1XXFlashRegState),
    .class_init    = stm32f1xx_flash_reg_class_init,
    .instance_init = stm32f1xx_flash_reg_init,
};

static void stm32f1xx_flash_reg_types(void)
{
    type_register_static(&stm32f1xx_flash_reg_info);
}

type_init(stm32f1xx_flash_reg_types);
