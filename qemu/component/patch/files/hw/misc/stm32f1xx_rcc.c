/*
 * STM32F1xx Reset and clock control (RCC) device
 *
 * Copyright (c) 2022 Michał Prochera <saturniak8888@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "qemu/osdep.h"
#include "qemu/module.h"
#include "qemu/error-report.h"
#include "qemu/log.h"
#include "migration/vmstate.h"
#include "hw/misc/stm32f1xx_rcc.h"
#include "hw/qdev-clock.h"

#define RCC_HSI_FRQ 8000000ULL
#define RCC_MAX_HCLK_FRQ 72000000ULL
#define RCC_MAX_APB1_PERIPH_FRQ 36000000ULL

#define RCC_CR_OFFSET 0x0
#define RCC_CFGR_OFFSET 0x4

#define RCC_CR_DEF_VAL 0x00000083
#define RCC_CFGR_DEF_VAL 0x00000000

#define RCC_CR_HSION_POS 0
#define RCC_CR_HSION_MSK 0x1

#define RCC_CR_HSIRDY_POS 1
#define RCC_CR_HSIRDY_MSK 0x1

#define RCC_CR_HSEON_POS 16
#define RCC_CR_HSEON_MSK 0x1

#define RCC_CR_HSERDY_POS 17
#define RCC_CR_HSERDY_MSK 0x1

#define RCC_CR_PLLON_POS 24
#define RCC_CR_PLLON_MSK 0x1

#define RCC_CR_PLLRDY_POS 25
#define RCC_CR_PLLRDY_MSK 0x1

#define RCC_CFGR_SW_POS 0
#define RCC_CFGR_SW_MSK 0x3
#define RCC_CFGR_SW_VAL_HSI 0x0
#define RCC_CFGR_SW_VAL_HSE 0x1
#define RCC_CFGR_SW_VAL_PLL 0x2

#define RCC_CFGR_SWS_POS 2
#define RCC_CFGR_SWS_MSK 0x3

#define RCC_CFGR_HPRE_POS 4
#define RCC_CFGR_HPRE_MSK 0xF
#define RCC_CFGR_HPRE_VAL_DIV2 0x8
#define RCC_CFGR_HPRE_VAL_DIV64 0xC

#define RCC_CFGR_PPRE1_POS 8
#define RCC_CFGR_PPRE1_MSK 0x7
#define RCC_CFGR_PPRE2_POS 11
#define RCC_CFGR_PPRE2_MSK 0x7
#define RCC_CFGR_PPRE_VAL_DIV2 0x4

#define RCC_CFGR_PLLSRC_POS 16
#define RCC_CFGR_PLLSRC_MSK 0x1
#define RCC_CFGR_PLLSRC_VAL_HSI 0x0
#define RCC_CFGR_PLLSRC_VAL_HSE 0x1

#define RCC_CFGR_PLLXTPRE_POS 17
#define RCC_CFGR_PLLXTPRE_MSK 0x1
#define RCC_CFGR_PLLXTPRE_VAL_NO_DIV 0x0
#define RCC_CFGR_PLLXTPRE_VAL_DIV2 0x1

#define RCC_CFGR_PLLMUL_POS 18
#define RCC_CFGR_PLLMUL_MSK 0xF
#define RCC_CFGR_PLLMUL_MUL_MIN 2
#define RCC_CFGR_PLLMUL_MUL_MAX 16

#define SET_VALUE(REG_NAME, VAL_NAME, VAL) \
    REG_NAME = (REG_NAME & ~(VAL_NAME##_MSK << VAL_NAME##_POS)) \
        | ((VAL & VAL_NAME##_MSK) << VAL_NAME##_POS)

#define GET_VALUE(REG_NAME, VAL_NAME) \
    ((REG_NAME >> VAL_NAME##_POS) & VAL_NAME##_MSK)

static inline void stm32f1xx_rcc_calc_single_apb_periods(
    uint64_t hclk_period, uint8_t apb_presc_val,
    uint64_t *out_apb_periph_period, uint64_t *out_apb_timer_period)
{
    *out_apb_periph_period = hclk_period;
    *out_apb_timer_period = hclk_period;
    if (apb_presc_val >= RCC_CFGR_PPRE_VAL_DIV2) {
        *out_apb_periph_period <<= (apb_presc_val - RCC_CFGR_PPRE_VAL_DIV2 + 1);
        *out_apb_timer_period = *out_apb_periph_period >> 1;
    }
}

static inline bool stm32f1xx_rcc_calc_pll_in_period(
    uint32_t rcc_cfgr, uint32_t old_rcc_cfgr,
    uint8_t hsi_on_val, uint8_t hse_on_val,
    uint8_t pll_on_val, uint64_t hsi_period,
    uint64_t hse_period, uint64_t *out_pll_in_period)
{
    uint8_t pll_src_val = 0;
    uint8_t pll_hse_presc_val = 0;
    uint8_t old_pll_src_val = 0;
    uint8_t old_pll_hse_presc_val = 0;

    pll_src_val = GET_VALUE(rcc_cfgr, RCC_CFGR_PLLSRC);
    old_pll_src_val = GET_VALUE(old_rcc_cfgr, RCC_CFGR_PLLSRC);

    if ((pll_on_val == 1) && (pll_src_val != old_pll_src_val)) {
        return false;
    }
    if (pll_src_val == RCC_CFGR_PLLSRC_VAL_HSI) {
        if (hsi_on_val == 0) {
            return false;
        }
        *out_pll_in_period = hsi_period << 1;
    } else {
        if (hse_on_val == 0) {
            return false;
        }
        pll_hse_presc_val = GET_VALUE(rcc_cfgr, RCC_CFGR_PLLXTPRE);
        old_pll_hse_presc_val = GET_VALUE(old_rcc_cfgr, RCC_CFGR_PLLXTPRE);
        if ((pll_on_val == 1) && (pll_hse_presc_val != old_pll_hse_presc_val)) {
            return false;
        }
        *out_pll_in_period = hse_period;
        if (pll_hse_presc_val == RCC_CFGR_PLLXTPRE_VAL_DIV2) {
            *out_pll_in_period <<= 1;
        }
    }
    return true;
}

static inline bool stm32f1xx_rcc_calc_pll_period(
    uint32_t rcc_cfgr, uint32_t old_rcc_cfgr,
    uint8_t pll_on_val, uint64_t pll_in_period,
    uint64_t *out_pll_period)
{
    uint8_t pll_mull_val = GET_VALUE(rcc_cfgr, RCC_CFGR_PLLMUL);
    uint8_t old_pll_mull_val = GET_VALUE(old_rcc_cfgr, RCC_CFGR_PLLMUL);

    if ((pll_on_val == 1) && (pll_mull_val != old_pll_mull_val)) {
        return false;
    }
    *out_pll_period = pll_in_period / MIN(
        pll_mull_val + RCC_CFGR_PLLMUL_MUL_MIN,
        RCC_CFGR_PLLMUL_MUL_MAX);
    return true;
}

static inline bool stm32f1xx_rcc_calc_sysclk_period(
    uint32_t rcc_cfgr, uint8_t hsi_on_val, uint8_t hse_on_val,
    uint8_t pll_on_val, uint64_t hsi_period,
    uint64_t hse_period, uint64_t pll_period,
    uint8_t *out_sw_val, uint64_t *out_sysclk_period)
{
    *out_sw_val = GET_VALUE(rcc_cfgr, RCC_CFGR_SW);
    switch (*out_sw_val) {
    case RCC_CFGR_SW_VAL_HSI:
        if (hsi_on_val == 0) {
            return false;
        }
        *out_sysclk_period = hsi_period;
        break;
    case RCC_CFGR_SW_VAL_HSE:
        if (hse_on_val == 0) {
            return false;
        }
        *out_sysclk_period = hse_period;
        break;
    case RCC_CFGR_SW_VAL_PLL:
        if (pll_on_val == 0) {
            return false;
        }
        *out_sysclk_period = pll_period;
        break;
    default:
        return false;
    }
    return true;
}

static inline void stm32f1xx_rcc_calc_hclk_period(
    uint32_t rcc_cfgr, uint64_t sysclk_period,
    uint64_t *out_hclk_period)
{
    uint8_t ahb_presc_val = GET_VALUE(rcc_cfgr, RCC_CFGR_HPRE);

    *out_hclk_period = sysclk_period;
    if (ahb_presc_val >= RCC_CFGR_HPRE_VAL_DIV2) {
        *out_hclk_period <<= (ahb_presc_val - RCC_CFGR_HPRE_VAL_DIV2 + 1);
    }
    if (ahb_presc_val >= RCC_CFGR_HPRE_VAL_DIV64) {
        *out_hclk_period <<= 1;
    }
}

static inline void stm32f1xx_rcc_calc_apb12_periods(
    uint32_t rcc_cfgr, uint64_t hclk_period,
    uint64_t *out_apb1_periph_period, uint64_t *out_apb1_timer_period,
    uint64_t *out_apb2_periph_period, uint64_t *out_apb2_timer_period)
{
    uint8_t apb1_presc_val = GET_VALUE(rcc_cfgr, RCC_CFGR_PPRE1);
    uint8_t apb2_presc_val = GET_VALUE(rcc_cfgr, RCC_CFGR_PPRE2);

    stm32f1xx_rcc_calc_single_apb_periods(hclk_period, apb1_presc_val,
        out_apb1_periph_period, out_apb1_timer_period);

    stm32f1xx_rcc_calc_single_apb_periods(hclk_period, apb2_presc_val,
        out_apb2_periph_period, out_apb2_timer_period);
}

static inline void stm32f1xx_rcc_update_regs(STM32F1XXRCCState *s,
    uint32_t rcc_cr, uint32_t rcc_cfgr, uint8_t hsi_on_val,
    uint8_t hse_on_val, uint8_t pll_on_val, uint8_t sw_val)
{
    uint32_t new_rcc_cr = rcc_cr;
    uint32_t new_rcc_cfgr = rcc_cfgr;
    SET_VALUE(new_rcc_cr, RCC_CR_HSIRDY, hsi_on_val);
    SET_VALUE(new_rcc_cr, RCC_CR_HSERDY, hse_on_val);
    SET_VALUE(new_rcc_cr, RCC_CR_PLLRDY, pll_on_val);
    SET_VALUE(new_rcc_cfgr, RCC_CFGR_SWS, sw_val);
    s->rcc_cr = new_rcc_cr;
    s->rcc_cfgr = new_rcc_cfgr;
}

static inline void stm32f1xx_rcc_update_clocks(STM32F1XXRCCState *s,
    uint64_t hclk_period, uint64_t apb1_periph_period,
    uint64_t apb1_timer_period, uint64_t apb2_periph_period,
    uint64_t apb2_timer_period)
{
    clock_update(s->hclk, hclk_period);
    clock_update(s->apb1_periph_clk, apb1_periph_period);
    clock_update(s->apb1_timer_clk, apb1_timer_period);
    clock_update(s->apb2_periph_clk, apb2_periph_period);
    clock_update(s->apb2_timer_clk, apb2_timer_period);
}

static bool stm32f1xx_rcc_apply_change(STM32F1XXRCCState *s,
    uint32_t rcc_cr, uint32_t rcc_cfgr)
{
    /*
     * Inside this subroutine clock periods
     * are calculated from incoming and current
     * values of RCC registers. If the provided 
     * values are valid, registers and clocks 
     * are updated.
     */

    uint64_t hsi_period = 0;
    uint64_t hse_period = 0;
    uint64_t pll_in_period = 0;
    uint64_t pll_period = 0;
    uint64_t sysclk_period = 0;
    uint64_t hclk_period = 0;
    uint64_t apb1_periph_period = 0;
    uint64_t apb1_timer_period = 0;
    uint64_t apb2_periph_period = 0;
    uint64_t apb2_timer_period = 0;

    uint8_t hsi_on_val = 0;
    uint8_t hse_on_val = 0;
    uint8_t pll_on_val = 0;
    uint8_t sw_val = 0;

    hsi_on_val = GET_VALUE(rcc_cr, RCC_CR_HSION);
    hse_on_val = GET_VALUE(rcc_cr, RCC_CR_HSEON);
    pll_on_val = GET_VALUE(rcc_cr, RCC_CR_PLLON);

    hsi_period = CLOCK_PERIOD_FROM_HZ(RCC_HSI_FRQ);
    if ((hse_on_val == 1) && (!clock_has_source(s->hse_clk))) {
        qemu_log_mask(LOG_GUEST_ERROR, "%s: "
            "Refusing to activate disconnected "
            "HSE clock.\n", __func__);
        return false;
    }
    hse_period = clock_get(s->hse_clk);

    if (!stm32f1xx_rcc_calc_pll_in_period(
        rcc_cfgr, s->rcc_cfgr,
        hsi_on_val, hse_on_val, pll_on_val,
        hsi_period, hse_period,
        &pll_in_period)) {
        qemu_log_mask(LOG_GUEST_ERROR, "%s: "
            "Refusing to configure the PLL source.",
            __func__);
        return false;
    }
    if (!stm32f1xx_rcc_calc_pll_period(
        rcc_cfgr, s->rcc_cfgr, pll_on_val,
        pll_in_period, &pll_period)) {
        qemu_log_mask(LOG_GUEST_ERROR, "%s: "
            "Refusing to change the PLL multiplier "
            "while PLL is active.\n", __func__);
        return false;
    }
    if (!stm32f1xx_rcc_calc_sysclk_period(
        rcc_cfgr, hsi_on_val, hse_on_val,
        pll_on_val, hsi_period, hse_period,
        pll_period, &sw_val, &sysclk_period)) {
        qemu_log_mask(LOG_GUEST_ERROR, "%s: "
            "Refusing to select inactive or invalid "
            "clock for SYSCLK.\n", __func__);
        return false;
    }
    stm32f1xx_rcc_calc_hclk_period(
        rcc_cfgr, sysclk_period, &hclk_period);
    stm32f1xx_rcc_calc_apb12_periods(
        rcc_cfgr, hclk_period,
        &apb1_periph_period, &apb1_timer_period,
        &apb2_periph_period, &apb2_timer_period);

    /*
     *  Allowing to set too high frequency because
     *  the STM32 HAL library expects that behavior.
     *
     *  See UTILS_EnablePLLAndSwitchSystem() function
     *  from STM32 HAL library for reference.
     */
    if (CLOCK_PERIOD_FROM_HZ(hclk_period)
        > RCC_MAX_HCLK_FRQ) {
        qemu_log_mask(LOG_GUEST_ERROR, "%s: "
            "The HCLK freqency is too high! "
            "Current = %llu Hz; Maximum = %llu Hz\n",
            __func__, CLOCK_PERIOD_TO_HZ(hclk_period),
            RCC_MAX_HCLK_FRQ);
    }
    if (CLOCK_PERIOD_FROM_HZ(apb1_periph_period)
        > RCC_MAX_APB1_PERIPH_FRQ) {
        qemu_log_mask(LOG_GUEST_ERROR, "%s: "
            "The APB1 peripheral freqency is too high! "
            "Current = %llu Hz; Maximum = %llu Hz\n",
            __func__, CLOCK_PERIOD_TO_HZ(apb1_periph_period),
            RCC_MAX_APB1_PERIPH_FRQ);
    }

    stm32f1xx_rcc_update_regs(s, rcc_cr, rcc_cfgr,
        hsi_on_val, hse_on_val, pll_on_val, sw_val);
    stm32f1xx_rcc_update_clocks(s, hclk_period,
        apb1_periph_period, apb1_timer_period,
        apb2_periph_period, apb2_timer_period);

    return true;
}

static uint64_t stm32f1xx_rcc_read(void *opaque,
    hwaddr addr, unsigned size)
{
    STM32F1XXRCCState *s = opaque;

    switch (addr) {
    case RCC_CR_OFFSET:
        return s->rcc_cr;
    case RCC_CFGR_OFFSET:
        return s->rcc_cfgr;
    }
    return 0;
}

static void stm32f1xx_rcc_write(void *opaque,
    hwaddr addr, uint64_t data, unsigned size)
{
    STM32F1XXRCCState *s = opaque;
    uint32_t rcc_cr = s->rcc_cr;
    uint32_t rcc_cfgr = s->rcc_cfgr;

    switch (addr) {
    case RCC_CR_OFFSET:
        rcc_cr = data;
        break;
    case RCC_CFGR_OFFSET:
        rcc_cfgr = data;
        break;
    }

    stm32f1xx_rcc_apply_change(s,
        rcc_cr, rcc_cfgr);
}

static const MemoryRegionOps stm32f1xx_rcc_ops = {
    .read = stm32f1xx_rcc_read,
    .write = stm32f1xx_rcc_write,
    .endianness = DEVICE_NATIVE_ENDIAN,
    .valid = {
        .min_access_size = 4,
        .max_access_size = 4,
        .unaligned = false
    }
};

static void stm32f1xx_rcc_on_hse_update(void *opaque, ClockEvent event)
{
    STM32F1XXRCCState *s = opaque;

    stm32f1xx_rcc_apply_change(s, s->rcc_cr, s->rcc_cfgr);
}

static void stm32f1xx_rcc_init(Object *obj)
{
    STM32F1XXRCCState *s = STM32F1XX_RCC(obj);

    s->hse_clk = qdev_init_clock_in(
        DEVICE(s), "hse-clk",
        stm32f1xx_rcc_on_hse_update, s,
        ClockUpdate);

    s->hclk = qdev_init_clock_out(DEVICE(s), "hclk");
    s->apb1_periph_clk = qdev_init_clock_out(DEVICE(s), "apb1-periph-clk");
    s->apb1_timer_clk = qdev_init_clock_out(DEVICE(s), "apb1-timer-clk");
    s->apb2_periph_clk = qdev_init_clock_out(DEVICE(s), "apb2-periph-clk");
    s->apb2_timer_clk = qdev_init_clock_out(DEVICE(s), "apb2-timer-clk");

    memory_region_init_io(&s->iomem, obj,
        &stm32f1xx_rcc_ops, s,
        TYPE_STM32F1XX_RCC, 0x400);
    sysbus_init_mmio(SYS_BUS_DEVICE(obj), &s->iomem);
}

static void stm32f1xx_rcc_realize(DeviceState *dev, Error **errp)
{
    STM32F1XXRCCState *s = STM32F1XX_RCC(dev);

    clock_set_hz(s->hclk, RCC_HSI_FRQ);
    clock_set_hz(s->apb1_periph_clk, RCC_HSI_FRQ);
    clock_set_hz(s->apb1_timer_clk, RCC_HSI_FRQ);
    clock_set_hz(s->apb2_periph_clk, RCC_HSI_FRQ);
    clock_set_hz(s->apb2_timer_clk, RCC_HSI_FRQ);
}

static int stm32f1xx_rcc_migr_post_load(void *opaque, int version_id)
{
    STM32F1XXRCCState *s = opaque;

    uint32_t rcc_cr = s->rcc_cr;
    uint32_t rcc_cfgr = s->rcc_cfgr;

    s->rcc_cr = RCC_CR_DEF_VAL;
    s->rcc_cfgr = RCC_CFGR_DEF_VAL;

    if (!stm32f1xx_rcc_apply_change(s, rcc_cr, rcc_cfgr)) {
        error_report(
            "%s: Restored RCC registers seem to be corrupted", __func__);
        return -1;
    }
    return 0;
}

static const VMStateDescription stm32f1xx_rcc_vmstate = {
    .name = TYPE_STM32F1XX_RCC,
    .version_id = 1,
    .minimum_version_id = 1,
    .post_load = stm32f1xx_rcc_migr_post_load,
    .fields = (VMStateField[]) {
        VMSTATE_UINT32(rcc_cr, STM32F1XXRCCState),
        VMSTATE_UINT32(rcc_cfgr, STM32F1XXRCCState),
        VMSTATE_END_OF_LIST()
    }
};

static void stm32f1xx_rcc_reset_enter(Object *obj, ResetType type)
{
    STM32F1XXRCCState *s = STM32F1XX_RCC(obj);

    s->rcc_cr = RCC_CR_DEF_VAL;
    s->rcc_cfgr = RCC_CFGR_DEF_VAL;
}

static void stm32f1xx_rcc_reset_hold(Object *obj)
{
    STM32F1XXRCCState *s = STM32F1XX_RCC(obj);

    clock_update_hz(s->hclk, RCC_HSI_FRQ);
    clock_update_hz(s->apb1_periph_clk, RCC_HSI_FRQ);
    clock_update_hz(s->apb1_timer_clk, RCC_HSI_FRQ);
    clock_update_hz(s->apb2_periph_clk, RCC_HSI_FRQ);
    clock_update_hz(s->apb2_timer_clk, RCC_HSI_FRQ);
}

static void stm32f1xx_rcc_class_init(ObjectClass *klass, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(klass);
    ResettableClass *rc = RESETTABLE_CLASS(klass);

    dc->realize = stm32f1xx_rcc_realize;
    dc->vmsd = &stm32f1xx_rcc_vmstate;
    rc->phases.enter = stm32f1xx_rcc_reset_enter;
    rc->phases.hold = stm32f1xx_rcc_reset_hold;
}

static const TypeInfo stm32f1xx_rcc_info = {
    .name          = TYPE_STM32F1XX_RCC,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(STM32F1XXRCCState),
    .class_init    = stm32f1xx_rcc_class_init,
    .instance_init = stm32f1xx_rcc_init,
};

static void stm32f1xx_rcc_types(void)
{
    type_register_static(&stm32f1xx_rcc_info);
}

type_init(stm32f1xx_rcc_types);
