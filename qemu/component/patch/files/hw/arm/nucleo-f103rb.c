/*
 * ST Nucleo F103RB machine
 *
 * Copyright (c) 2022 Michał Prochera <saturniak8888@gmail.com>
 * Copyright (c) 2021 Alexandre Iooss <erdnaxe@crans.org>
 * Copyright (c) 2014 Alistair Francis <alistair@alistair23.me>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "qemu/osdep.h"
#include "qapi/error.h"
#include "hw/boards.h"
#include "hw/qdev-clock.h"
#include "hw/arm/boot.h"
#include "hw/arm/stm32f103_soc.h"

#define HSE_CLK_FRQ 8000000ULL

/* nucleof103rb implementation is derived from stm32vldiscovery */

static void nucleof103rb_init(MachineState *machine)
{
    DeviceState *dev;
    Clock *hse_clk;

    hse_clk = clock_new(OBJECT(machine), "hse-clk");
    clock_set_hz(hse_clk, HSE_CLK_FRQ);

    dev = qdev_new(TYPE_STM32F103_SOC);
    qdev_connect_clock_in(dev, "hse-clk", hse_clk);
    sysbus_realize_and_unref(SYS_BUS_DEVICE(dev), &error_fatal);

    armv7m_load_kernel(ARM_CPU(first_cpu),
        machine->kernel_filename,
        FLASH_SIZE);
}

static void nucleof103rb_machine_init(MachineClass *mc)
{
    mc->desc = "ST Nucleo F103RB (Cortex-M3)";
    mc->init = nucleof103rb_init;
}

DEFINE_MACHINE("nucleof103rb", nucleof103rb_machine_init)
