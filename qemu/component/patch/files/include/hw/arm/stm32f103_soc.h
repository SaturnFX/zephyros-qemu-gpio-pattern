/*
 * STM32F103 SoC
 *
 * Copyright (c) 2022 Michał Prochera <saturniak8888@gmail.com>
 * Copyright (c) 2021 Alexandre Iooss <erdnaxe@crans.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef HW_ARM_STM32F103_SOC_H
#define HW_ARM_STM32F103_SOC_H

#include "qom/object.h"
#include "hw/clock.h"
#include "hw/arm/armv7m.h"
#include "hw/misc/stm32f1xx_flash_reg.h"
#include "hw/misc/stm32f1xx_rcc.h"
#include "hw/char/stm32f2xx_usart.h"
#include "hw/ssi/stm32f2xx_spi.h"
#include "hw/gpio/stm32f1xx_gpio.h"

#define TYPE_STM32F103_SOC "stm32f103-soc"
OBJECT_DECLARE_SIMPLE_TYPE(STM32F103State, STM32F103_SOC)

#define STM_NUM_USARTS 3
#define STM_NUM_SPIS 2
#define STM_NUM_GPIOS 5

#define FLASH_BASE_ADDRESS 0x08000000
#define FLASH_SIZE (128 * 1024)
#define SRAM_BASE_ADDRESS 0x20000000
#define SRAM_SIZE (20 * 1024)

struct STM32F103State {
    /*< private >*/
    SysBusDevice parent_obj;

    /*< public >*/
    Clock *hse_clk;

    Clock *hclk_div8;

    ARMv7MState armv7m;

    MemoryRegion sram;
    MemoryRegion flash;
    MemoryRegion flash_alias;

    STM32F1XXFlashRegState flash_reg;
    STM32F1XXRCCState rcc;
    STM32F2XXUsartState usart[STM_NUM_USARTS];
    STM32F2XXSPIState spi[STM_NUM_SPIS];
    STM32F1XXGPIOState gpio[STM_NUM_GPIOS];
};

#endif
