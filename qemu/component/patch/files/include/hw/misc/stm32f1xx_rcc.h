/*
 * STM32F1xx Reset and clock control (RCC) device
 *
 * Copyright (c) 2022 Michał Prochera <saturniak8888@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef HW_MISC_STM32F1XX_RCC_H
#define HW_MISC_STM32F1XX_RCC_H

#include "qom/object.h"
#include "hw/sysbus.h"
#include "hw/clock.h"

#define TYPE_STM32F1XX_RCC "stm32f1xx-rcc"
OBJECT_DECLARE_SIMPLE_TYPE(STM32F1XXRCCState, STM32F1XX_RCC)

struct STM32F1XXRCCState {
    /*< private >*/
    SysBusDevice parent_obj;

    /*< public >*/
    MemoryRegion iomem;

    Clock *hse_clk;

    Clock *hclk;
    Clock *apb1_periph_clk;
    Clock *apb1_timer_clk;
    Clock *apb2_periph_clk;
    Clock *apb2_timer_clk;

    uint32_t rcc_cr;
    uint32_t rcc_cfgr;
};

#endif
