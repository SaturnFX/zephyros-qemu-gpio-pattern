/*
 * STM32F1xx GPIO registers device
 *
 * Copyright (c) 2022 Michał Prochera <saturniak8888@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef HW_GPIO_STM32F1XX_GPIO_H
#define HW_GPIO_STM32F1XX_GPIO_H

#include "qom/object.h"
#include "hw/sysbus.h"

#define TYPE_STM32F1XX_GPIO "stm32f1xx-gpio"
OBJECT_DECLARE_SIMPLE_TYPE(STM32F1XXGPIOState, STM32F1XX_GPIO)

struct STM32F1XXGPIOState {
    /*< private >*/
    SysBusDevice parent_obj;

    /*< public >*/
    MemoryRegion iomem;

    uint32_t gpio_crl;
    uint32_t gpio_crh;
    uint32_t gpio_idr;
    uint32_t gpio_odr;
};

#endif
