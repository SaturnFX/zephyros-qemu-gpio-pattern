#!/bin/bash
set -e
ZEPHYR_REPO="zephyrproject-rtos/zephyr"
ZEPHYR_COMMIT="8a4c58618bc4a4af8231445e265e8927b2577956"
ZEPHYR_TARBALL_URL="https://api.github.com/repos/${ZEPHYR_REPO}/tarball/${ZEPHYR_COMMIT}"
echo "${ZEPHYR_TARBALL_URL}"
