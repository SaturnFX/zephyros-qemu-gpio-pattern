# Copyright (c) 2022 Michał Prochera
#
# SPDX-License-Identifier: Apache-2.0

import socket
from pathlib import Path
from typing import Optional, Union


class QTestClient:
    # Inspired by python/qemu/machine/qtest.py 
    # script from QEMU repository

    def __init__(self, sock_path: Union[Path, str]) -> None:
        self._sock = socket.socket(
            socket.AF_UNIX, socket.SOCK_STREAM)
        try:
            self._sock.connect(str(sock_path))
            self._file = self._sock.makefile(
                mode='rw', encoding='utf-8')
        except:
            self._sock.close()
            raise ConnectionError(
                'Could not establish a connection')
    
    def __enter__(self) -> 'QTestClient':
        return self

    def __exit__(self, *args) -> None:
        self.close()

    def _make_reqest(self, cmd: str) -> Optional[str]:
        assert self._file is not None
        self._file.write(cmd + '\n')
        self._file.flush()
        data = self._file.readline()
        parts = data.rstrip().split(' ', maxsplit=1)
        if parts[0] != "OK":
            raise ProtocolError(*parts)
        return parts[1] if len(parts) > 1 else None

    def read_mem(self, addr: int, size: int) -> bytes:
        data_str = self._make_reqest(
            f'read 0x{addr:x} 0x{size:x}')
        assert data_str[:2] in ['0x', '0X']
        return bytes.fromhex(data_str[2:])

    def write_mem(self, addr: int, size: int, data: bytes) -> None:
        self._make_reqest(
            f'write 0x{addr:x} 0x{size:x} 0x{data.hex()}')

    def close(self) -> None:
        if self._sock is not None:
            self._file.close()
            self._file = None
            self._sock.close()
            self._sock = None


class QTestError(Exception):
    pass


class ConnectionError(QTestError):
    pass


class ProtocolError(QTestError):
    def __init__(self, type: str, msg: str) -> None:
        self.type = type
        self.msg = msg

    def __str__(self) -> str:
        return f'(response: {self.type}) {self.msg}'
