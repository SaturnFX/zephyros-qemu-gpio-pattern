# Copyright (c) 2022 Michał Prochera
#
# SPDX-License-Identifier: Apache-2.0

import cmd
from textwrap import dedent
from types import FunctionType
from typing import Any, Callable, Dict, \
    List, Optional, Tuple


class ShellDedentMeta(type):
    dedent_attrs = ['doc_leader']

    def __new__(cls, name: str, bases: Tuple[type, ...], 
            dict: Dict[str, Any]) -> type:
        new_dict = {}
        for attr, val in dict.items():
            new_val = val

            if isinstance(val, FunctionType) \
                    and (val.__doc__ is not None):
                val.__doc__ = dedent(val.__doc__)
            
            if type(val) is str:
                if attr in cls.dedent_attrs:
                    new_val = dedent(val)
                elif attr[:5] == 'help_':
                    new_val = cls.make_help_func(val)

            new_dict[attr] = new_val
        return type.__new__(cls, name, bases, new_dict)
    
    @staticmethod
    def make_help_func(help) -> Callable[..., None]:
        def wrapper(*args, **kwargs) -> None:
            print(dedent(help))
        return wrapper


class SimShell(cmd.Cmd, metaclass=ShellDedentMeta):
    intro_help = 'Type help or ? to list commands'
    history_max = 100

    def cmdloop(self) -> None:
        try:
            super().cmdloop()
        except KeyboardInterrupt:
            print()
            print('Detected CTRL+C. Exiting...')
            self.postloop()

    def preloop(self) -> None:
        try:
            import readline
            history = self._get_history()
            if history is None:
                return
            readline.read_history_file(history)
        except ImportError:
            pass

    def postloop(self) -> None:
        try:
            import readline
            history = self._get_history()
            if history is None:
                return
            history_max = self.history_max
            readline.set_history_length(history_max)
            readline.write_history_file(history)
        except ImportError:
            pass

    def onecmd(self, line: str) -> bool:
        try:
            return super().onecmd(line)
        except BaseException as err:
            print(f'Error! {str(err)}')
            return False

    def emptyline(self) -> bool:
        if self.lastcmd:
            print(f'Repeating: \'{self.lastcmd}\'')
        return super().emptyline()

    def parseline(self, line: str) \
            -> Tuple[Optional[str], Optional[str], str]:
        cmd, arg, line = super().parseline(line)
        arg_array = arg.split(' ') if arg else []
        return cmd, arg_array, line

    def do_quit(self, args: List[str]) -> bool:
        '''Quits the shell'''
        print('Exiting...')
        return True

    def do_help(self, args: List[str]) -> None:
        help_arg = args[0] if len(args) > 0 else ''
        return super().do_help(help_arg)

    do_help.__doc__ = cmd.Cmd.do_help.__doc__

    def do_history(self, args: List[str]) -> None:
        '''Show shell usage history

           Usage: history [PRINT_LIMIT]
        '''
        if len(args) > 1:
            raise ValueError('Provided too many arguments')
        try:
            import readline
            offset = 1
            history_len = readline.get_current_history_length()
            if len(args) > 0:
                offset = max(offset, history_len - int(args[0]))
            for index in range(offset, history_len):
                print(readline.get_history_item(index))
        except ValueError:
            raise ValueError('Invalid history print limit')
        except ImportError:
            pass

    @classmethod
    def _get_history(cls) -> Optional[str]:
        history = getattr(cls, 'history', None)
        if history is None:
            return None
        if not history.is_file():
            history.parent.mkdir(parents=True, exist_ok=True)
            history.touch(exist_ok=False)
        return str(history)
