# Copyright (c) 2022 Michał Prochera
#
# SPDX-License-Identifier: Apache-2.0

import argparse

from west.commands import WestCommand
from west import log

from zcmake import run_build
from build_helpers import is_zephyr_build, find_build_dir, \
    FIND_BUILD_DIR_DESCRIPTION
from run_common import add_parser_common, do_run_common

RUN_DESCRIPTION = f'''
Runs existing Zephyr binary inside one of the supported emulators.

{FIND_BUILD_DIR_DESCRIPTION}
'''

SIM_DESCRIPTION = '''
Allows user to control peripheral devices of the VM launched 
by the \"west run\" command.
'''

def _banner(cmd, msg):
    log.inf(f'-- west {cmd}: {msg}', colorize=True)

class Run(WestCommand):

    def __init__(self):
        super().__init__(
            'run',
            # Keep this in sync with the string in west-commands.yml.
            'run a Zephyr binary e.g. inside the emulator',
            RUN_DESCRIPTION,
            accepts_unknown_args=False)

    def do_add_parser(self, parser_adder):
        parser = parser_adder.add_parser(
            self.name,
            help=self.help,
            formatter_class=argparse.RawDescriptionHelpFormatter,
            description=self.description)
        
        parser.add_argument('method', nargs='?', 
            metavar='METHOD', help='suffix of one of the run_* targets')

        parser.add_argument('-d', '--build-dir',
            metavar='BUILD_DIR', help='build directory with Zephyr binary')
        parser.add_argument('-n', '--just-print', '--dry-run', '--recon',
            dest='dry_run', action='store_true',
            help="just print commands; don't run them")
        
        return parser

    def do_run(self, args, unknown_args):
        _banner('run', 'running Zephyr binary' \
            + (' with ' + args.method if args.method else ''))
        
        build_dir = find_build_dir(args.build_dir)
        if not is_zephyr_build(build_dir):
            log.die(build_dir + ' is not a valid Zephyr build directory')

        extra_args = [
            '--target', 'run' + ('_' + args.method if args.method else '')
        ]
        run_build(build_dir, extra_args=extra_args, dry_run=args.dry_run)


class Simulate(WestCommand):

    def __init__(self):
        super(Simulate, self).__init__(
            'sim',
            # Keep this in sync with the string in west-commands.yml.
            'control ZephyrOS VM peripheral devices',
            SIM_DESCRIPTION,
            accepts_unknown_args=True)
        self.runner_key = 'sim-runner'  # in runners.yaml

    def do_add_parser(self, parser_adder):
        return add_parser_common(self, parser_adder)

    def do_run(self, my_args, runner_args):
        my_args.skip_rebuild = True
        do_run_common(self, my_args, runner_args)
