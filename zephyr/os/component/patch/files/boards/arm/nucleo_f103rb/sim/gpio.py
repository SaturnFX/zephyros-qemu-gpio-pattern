# Copyright (c) 2022 Michał Prochera
#
# SPDX-License-Identifier: Apache-2.0

from pathlib import Path
from colorama import Fore, Style
from typing import Iterable, List, Tuple

from simutils.shell import SimShell
from simutils.qtest import QTestClient

GPIO_PORT_ADDR = {
    'A': 0x40010800, 
    'B': 0x40010C00, 
    'C': 0x40011000,
    'D': 0x40011400, 
    'E': 0x40011800
}
GPIO_PIN_MAX = 15

GPIO_IDR_ADDR = 0x08
GPIO_ODR_ADDR = 0x0C


def run_sim(qtest: QTestClient) -> None:
    sim_shell = GPIOSimShell(qtest)
    sim_shell.cmdloop()


class GPIOSimShell(SimShell):
    prompt = f'({Fore.GREEN}gpio{Style.RESET_ALL}) '
    intro = Fore.GREEN \
        + 'GPIO simulation shell' \
        + Style.RESET_ALL + '\n\n' \
        + SimShell.intro_help \
        + '\n'
    history = Path.home() / '.zephyr_qemu_gpio_hist'
    doc_leader = f'''
        {Fore.GREEN}GPIO simulation shell{Style.RESET_ALL}
     
        Allows to read and write GPIO pin values
        from a running QEMU VM.
    '''
    help_GPIO_EXPR = '''
        GPIO_EXPR has a form of P{PORT}{PIN} where:
        * PORT is GPIO port - a char from 'A' to 'E'
        * PIN is GPIO pin - a number from 0 to 15 (inclusive)

        Additionally, the PORT or PIN can be replaced 
        with '*' char, which means to match any port or pin. 
        The same effect can be achieved when skipping 
        the PIN or PORT and PIN entirely.
    '''

    def __init__(self, client: QTestClient) -> None:
        super().__init__()
        self.client = client

    def do_read(self, args: List[str]) -> None:
        '''
            Reads value from specified GPIO pins

            Usage: read [GPIO_EXPR ...]

            No arguments means to read all GPIO pins.
        '''
        gpios = set()
        expr_list = [arg.upper() for arg in args]
        if len(args) == 0:
            expr_list = ['']
        for expr in expr_list:
            gpios.update(self._get_gpios(expr, expr))

        results = []
        for gpio in sorted(gpios):
            val = self._read_gpio(gpio)
            results.append(f'P{gpio[0]}{gpio[1]} = {val}')
        if len(results) == 1:
            print(results[0])
            return
        self.columnize(results)

    def do_write(self, args: List[str]) -> None:
        '''
            Writes a value to specified GPIO pins

            Usage: write GPIO_EXPR={0 | 1} ...
        '''
        if len(args) == 0:
            raise ValueError('Provide at least one GPIO with value')
        for expr in [arg.upper() for arg in args]:
            gpio_expr, val = self._get_assign(expr)
            for gpio in self._get_gpios(gpio_expr, expr):
                self._write_gpio(gpio, val)

    def _read_gpio(self, gpio: Tuple[str, int]) -> int:
        addr = GPIO_PORT_ADDR[gpio[0]] + GPIO_ODR_ADDR
        val = self._read_reg(addr)
        return (val & (1 << gpio[1])) >> gpio[1]

    def _write_gpio(self, gpio: Tuple[str, int], value: int) -> None:
        addr = GPIO_PORT_ADDR[gpio[0]] + GPIO_IDR_ADDR
        in_val = self._read_reg(addr)
        out_val = (in_val & ~(1 << gpio[1])) \
             | ((value & 1) << gpio[1])
        self._write_reg(addr, out_val)

    def _read_reg(self, addr: int) -> int:
        mem_bytes = self.client.read_mem(addr, 4)
        return int.from_bytes(mem_bytes, 'little')

    def _write_reg(self, addr: int, val: int) -> None:
        mem_bytes = val.to_bytes(4, 'little')
        self.client.write_mem(addr, 4, mem_bytes)

    @staticmethod
    def _get_gpios(expr: str, full: str) -> List[Tuple[str, int]]:
        gpios = []
        if (len(expr) > 0) and (expr[0] != 'P'):
            raise ValueError(f'{full} has invalid prefix')
        ports = GPIOSimShell._get_ports(expr, full)
        pins = GPIOSimShell._get_pins(expr, full)
        for port in ports:
            for pin in pins:
                gpios.append((port, pin))
        return gpios

    @staticmethod
    def _get_ports(expr: str, full: str) -> Iterable[str]:
        port_expr = '' if len(expr) < 2 else expr[1]
        if port_expr in GPIO_PORT_ADDR:
            return [port_expr]
        elif port_expr in ['*', '']:
            return GPIO_PORT_ADDR.keys()
        else:
            raise ValueError(f'{full} has invalid port name')

    @staticmethod
    def _get_pins(expr: str, full: str) -> Iterable[int]:
        pin_expr = '' if len(expr) < 3 else expr[2:]
        if pin_expr in ['*', '']:
            return range(0, GPIO_PIN_MAX + 1)
        else:
            try:
                pin_num = int(pin_expr)
                if (pin_num < 0) or (pin_num > GPIO_PIN_MAX): 
                    raise ValueError
            except:
                raise ValueError(f'{full} has invalid pin number')
            return [pin_num]
    
    @staticmethod
    def _get_assign(expr: str) -> Tuple[str, int]:
        assign_expr = expr.split('=')
        if len(assign_expr) != 2:
            raise ValueError(f'{expr} has invalid assignment')
        try:
            assign_num = int(assign_expr[1])
            if assign_num not in [0, 1]: 
                raise ValueError
        except:
            raise ValueError(f'{expr} assignees invalid value')
        return assign_expr[0], assign_num
