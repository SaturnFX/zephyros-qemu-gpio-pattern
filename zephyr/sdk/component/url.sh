#!/bin/bash
set -e
SDK_REPO="zephyrproject-rtos/sdk-ng"
SDK_API_VERSION="53612113"
SDK_API_URL="https://api.github.com/repos/${SDK_REPO}/releases/${SDK_API_VERSION}"
SDK_TARBALL_URL="$(wget -qO- "${SDK_API_URL}" \
    | sed -rn 's|^\s*"tarball_url"\s*:\s*"(.+)",{0,1}\s*$|\1|p')"
echo "${SDK_TARBALL_URL}"
