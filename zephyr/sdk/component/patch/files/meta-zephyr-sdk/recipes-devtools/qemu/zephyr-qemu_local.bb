
DEPENDS = "glib-2.0 zlib pixman gnutls libtasn1 dtc ninja-native meson-native"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=441c28d2cf86e15a37fa47e15a72fbac \
                    file://COPYING.LIB;endline=24;md5=8c5efda6cf1e1b03dcfd0e6c0d271c7f"
SRC_SUBDIR = "local"

SRC_URI = "file://${QEMU_SRC_DIR};subdir=${SRC_SUBDIR} \
    https://github.com/zephyrproject-rtos/seabios/releases/download/zephyr-v1.0.0/bios-128k.bin;name=bios-128k \
	https://github.com/zephyrproject-rtos/seabios/releases/download/zephyr-v1.0.0/bios-256k.bin;name=bios-256k \
    file://configure-cross.patch \  
  "

SRC_URI[bios-128k.sha256sum] = "943c077c3925ee7ec85601fb12937a0988c478a95523a628cd7e61c639dd6e81"
SRC_URI[bios-256k.sha256sum] = "19133167cc0bfb2a9e8ce9567efcd013a4ab80d2f3522ac66df0c23c68c18984"

BBCLASSEXTEND = "native nativesdk"
INHIBIT_PACKAGE_DEBUG_SPLIT = "1"
INHIBIT_PACKAGE_STRIP = "1"

S = "${WORKDIR}/${SRC_SUBDIR}"

inherit autotools pkgconfig

QEMUS_BUILT = "aarch64-softmmu arm-softmmu i386-softmmu mips-softmmu mipsel-softmmu nios2-softmmu xtensa-softmmu riscv32-softmmu riscv64-softmmu sparc-softmmu x86_64-softmmu"
QEMU_FLAGS = "--disable-docs  --disable-sdl --disable-debug-info  --disable-cap-ng \
  --disable-libnfs --disable-libusb --disable-libiscsi --disable-usb-redir --disable-linux-aio \
  --disable-guest-agent --disable-libssh --disable-vnc-png  --disable-seccomp \
  --disable-tpm  --disable-numa --disable-glusterfs \
  --disable-virtfs --disable-xen --disable-curl --disable-attr --disable-curses --disable-iconv \
  --disable-kvm --disable-parallels --disable-replication \
  --disable-live-block-migration --disable-dmg \
  "

def fix_src_path(d):
    # This function removes unnecessary 
    # input source path components 
    # from the workdir source path
    # left by BB do_unpack() task.

    import shutil
    from pathlib import Path

    def _find_src_dir(root):
        dir = root
        gitignore = dir / '.gitignore'
        while not gitignore.is_file():
            dir = next(dir.glob("*"), None)
            if dir is None:
                break
            gitignore = dir / '.gitignore'
        return dir

    work_dir = Path(d.getVar('WORKDIR'))
    src_dir = Path(d.getVar('S'))
    temp_dir = Path(d.getVar('WORKDIR')) / 'src-temp'

    real_src_dir = _find_src_dir(src_dir)

    real_src_dir.rename(temp_dir)
    shutil.rmtree(str(src_dir))
    temp_dir.rename(src_dir)

copy_seabios() {
    cp ${WORKDIR}/bios-128k.bin ${S}/pc-bios/bios.bin
    cp ${WORKDIR}/bios-256k.bin ${S}/pc-bios/bios-256k.bin
}

do_unpack:append() {
    fix_src_path(d)
    bb.build.exec_func('copy_seabios', d)
}

do_configure() {
    ${S}/configure ${QEMU_FLAGS} --target-list="${QEMUS_BUILT}" --prefix=${prefix}  \
        --sysconfdir=${sysconfdir} --libexecdir=${libexecdir} --localstatedir=${localstatedir} \
        --meson=meson
}

do_install:append() {
    ln -sf ../xilinx/bin/qemu-system-aarch64 ${D}${bindir}/qemu-system-xilinx-aarch64
    ln -sf ../xilinx/bin/qemu-system-microblazeel ${D}${bindir}/qemu-system-xilinx-microblazeel
}

FILES:${PN} += "${datadir}/icons ${datadir}/qemu"

INSANE_SKIP:${PN} = "already-stripped"
