#!/bin/bash
set -e
SDK_SETUP_PATH="$(echo "../component/out/zephyr-sdk"*".run")"

if [ ! -f "${SDK_SETUP_PATH}" ]; then
    echo "Error! SDK setup does not exist."
    exit 1
fi

if [ -z "${ZEPHYR_SDK_INSTALL_DIR}" ]; then
    echo "Error! ZEPHYR_SDK_INSTALL_DIR is not defined."
    exit 1
fi

echo "Installing SDK.."
chmod +x "${SDK_SETUP_PATH}"
"${SDK_SETUP_PATH}" -- -d "${ZEPHYR_SDK_INSTALL_DIR}" -norc
