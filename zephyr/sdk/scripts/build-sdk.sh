#!/bin/bash
set -e
setConfigVar() {
    local VAR_NAME="$1"; shift
    local VAR_VALUE="$1"; shift
    local CFG_PATH="$1"; shift

	sed -i "/^${VAR_NAME}=/d" "${CFG_PATH}"
	echo "${VAR_NAME}=${VAR_VALUE}" >> "${CFG_PATH}"
}

SDK_TARGETS=(
    "cmake"
    "arm"
    "arc"
    "riscv64"
    "nios2"
    "sparc"
    "x86_64-zephyr-elf"
    "arm64"
    "xtensa_sample_controller"
    "xtensa_intel_apl_adsp"
    "xtensa_intel_bdw_adsp"
    "xtensa_intel_byt_adsp"
    "xtensa_nxp_imx_adsp"
    "xtensa_nxp_imx8m_adsp"
    "xtensa_intel_s1000"
    "mips"
    "arc64"
    "tools"
)
SDK_IN_TARBALL_DIR="tarballs"
SDK_IN_TARBALL_URLS=(
    # Workaround for unavailable source that is used by ct-ng by default
    "https://mirrors.kernel.org/slackware/slackware64-current/source/l/isl/isl-0.24.tar.xz"
)

echo "Building SDK targets.."
cd ../component/src

if [ "$(id -u)" -eq 0 ] \
    && ! grep -E "^[0-9]+:.*:/docker/.+$" /proc/1/cgroup > /dev/null 2>&1; then
    echo "Error! You can not run this script as root user outside Docker."
    exit 1
fi

mkdir "${SDK_IN_TARBALL_DIR}"
for TARBALL_URL in "${SDK_IN_TARBALL_URLS[@]}"; do
    wget --directory-prefix="${SDK_IN_TARBALL_DIR}" "${TARBALL_URL}"
done

SDK_IN_TARBALL_DIR_ABS="$(readlink -e "${SDK_IN_TARBALL_DIR}")"
for SDK_TARGET in "${SDK_TARGETS[@]}"; do
    CONFIG_PATH="configs/${SDK_TARGET}.config"
    if [ -f "${CONFIG_PATH}" ]; then
        setConfigVar "CT_EXPERIMENTAL" "y" "${CONFIG_PATH}"
        setConfigVar "CT_ALLOW_BUILD_AS_ROOT" "y" "${CONFIG_PATH}"
        setConfigVar "CT_ALLOW_BUILD_AS_ROOT_SURE" "y" "${CONFIG_PATH}"
        setConfigVar "CT_LOCAL_TARBALLS_DIR" \
            "\"${SDK_IN_TARBALL_DIR_ABS}\"" "${CONFIG_PATH}"
        setConfigVar "CT_GDB_CROSS_PYTHON_BINARY" \
            "\"python3.9\"" "${CONFIG_PATH}"
    fi
    ./go.sh "${SDK_TARGET}"
done
