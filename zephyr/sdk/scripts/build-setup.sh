#!/bin/bash
set -e
SDK_OUT_DIR="../../out"

echo "Building SDK setup.."
cd ../component/src/scripts
mkdir toolchains
mv ../zephyr-sdk-*-hosttools-standalone-*.sh toolchains/
mv ../*.tar.bz2 toolchains/
./make_zephyr_sdk.sh "linux" "$(uname -m)"
mkdir -p "${SDK_OUT_DIR}"
mv -t "${SDK_OUT_DIR}" *.run 
