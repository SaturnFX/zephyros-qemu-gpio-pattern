#!/bin/bash
set -e

echo "Building SDK dependencies.."
cd ../component/src
./go.sh crosstool
