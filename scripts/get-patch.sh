#!/bin/bash
set -e
SCRIPT_DIR="$(dirname "${BASH_SOURCE}")"
. "${SCRIPT_DIR}/common.sh"
printHelpMessage() {
    echoWithoutIndent "
    Generates patch by comparing modified source code
    and the original one

    Syntax: $0 [options] <component_dir>

    Options:
    -s, --src-dir <src_dir>         Specifies custom source code directory
    -o, --org-dir <org_dir>         Specifies custom source code compare
                                    directory
    -p, --patch-dir <patch_dir>     Specifies custom patch directory                           
    -h, --help                      Print this message
    "
}

echo "Source patch generator script v1.0"

SRC_DIR=""
ORG_DIR=""
PATCH_DIR=""

PARSED_ARGS=$(LANG=C getopt --name "$0" --options="s:o:p:h" \
    --longoptions="src-dir:,org-dir:,patch-dir:,help" -- "$@") || exit 1
eval set -- "${PARSED_ARGS}"
while true; do
    case "$1" in
        -s|--src-dir)
            SRC_DIR="$2"
            shift 2
            ;;
        -o|--org-dir)
            ORG_DIR="$2"
            shift 2
            ;;
        -p|--patch-dir)
            PATCH_DIR="$2"
            shift 2
            ;;
        -h|--help)
            printHelpMessage
            exit 0
            ;;
        --)
            shift
            break
            ;;
        *)
            printHelpMessage
            exit 1
            ;;
    esac
done

if [ "$#" -ne 1 ] || [ ! -d "$1" ]; then
    echo "Error! No valid component directory has been provided."
    exit 1
fi

if [ -z "${SRC_DIR}" ]; then
    SRC_DIR="$1/${DEFAULT_SRC}"
fi
if [ -z "${ORG_DIR}" ]; then
    ORG_DIR="$1/${DEFAULT_ORG}"
fi
if [ -z "${PATCH_DIR}" ]; then
    PATCH_DIR="$1/${DEFAULT_PATCH}"
fi

if [ ! -d "${SRC_DIR}" ]; then
    echo "Error! Source directory does not exists."
    exit 1
fi
if [ ! -d "${ORG_DIR}" ]; then
    echo "Error! Original source directory does not exists."
    exit 1
fi
if ! isParentDirExits "${PATCH_DIR}"; then
    echo "Error! Specified patch diretory is invalid."
    exit 1
fi

echo

recreateDirectory "${PATCH_DIR}"

PATCH_FILES_DIR="${PATCH_DIR}/files"
PATCH_DELETED_FILE="${PATCH_DIR}/deleted.txt"

CHANGES_COUNTER=0
echo "Generating patch.."

while IFS= read -r -d '' CURR_FILE_REL_PATH; do
    CURR_SRC_FILE="${SRC_DIR}/${CURR_FILE_REL_PATH}"
    CURR_ORG_FILE="${ORG_DIR}/${CURR_FILE_REL_PATH}"
    CURR_PATCH_FILE_BASENAME="${PATCH_FILES_DIR}/${CURR_FILE_REL_PATH}"

    CHANGES_COUNTER=$((CHANGES_COUNTER + 1))
    if [ ! -f "${CURR_ORG_FILE}" ]; then
        echo "Copying new ${CURR_FILE_REL_PATH}.."

        if [[ "${CURR_PATCH_FILE_BASENAME}" == *.patch ]]; then
            CURR_PATCH_FILE_BASENAME="${CURR_PATCH_FILE_BASENAME}."
        fi
        mkdir -p "$(dirname "${CURR_PATCH_FILE_BASENAME}")"
        cp "${CURR_SRC_FILE}" "${CURR_PATCH_FILE_BASENAME}"
    elif [ ! -f "${CURR_SRC_FILE}" ]; then
        echo "Marking deleted ${CURR_FILE_REL_PATH}.."

        echo "${CURR_FILE_REL_PATH}" >> "${PATCH_DELETED_FILE}"
    else
        if ! cmp --silent "${CURR_ORG_FILE}" "${CURR_SRC_FILE}"; then
            echo "Creating patch for modified ${CURR_FILE_REL_PATH}.."

            mkdir -p "$(dirname "${CURR_PATCH_FILE_BASENAME}")"
            diff -u "${CURR_ORG_FILE}" "${CURR_SRC_FILE}" \
                > "${CURR_PATCH_FILE_BASENAME}.patch" || true 
        else
            CHANGES_COUNTER=$((CHANGES_COUNTER - 1))
        fi
    fi
done < <(
    {
        silentCd "${SRC_DIR}"
        find . -type f -print0
        silentCd -
        silentCd "${ORG_DIR}"
        find . -type f -print0
        silentCd -
    } | sed -z 's|^\./||' | sort -uz
)

echo "Done! Found ${CHANGES_COUNTER} file changed."
