#!/bin/bash
set -e
SCRIPT_DIR="$(dirname "${BASH_SOURCE}")"
. "${SCRIPT_DIR}/common.sh"
printHelpMessage() {
    echoWithoutIndent "
    Fetches the source code from external source

    Syntax: $0 [options] <component_dir>

    Options:
    -s, --src-dir <src_dir>       Specifies custom source code directory
    -o, --org-dir [org_dir]       Enables creating source code backup 
                                  and optionally specifies custom backup 
                                  directory
    -h, --help                    Print this message
    "
}

echo "Source getter script v1.0"

IS_ORG_ENABLED=0
SRC_DIR=""
ORG_DIR=""

PARSED_ARGS=$(LANG=C getopt --name "$0" --options="s:o::h" \
    --longoptions="src-dir:,org-dir::,help" -- "$@") || exit 1
eval set -- "${PARSED_ARGS}"
while true; do
    case "$1" in
        -s|--src-dir)
            SRC_DIR="$2"
            shift 2
            ;;
        -o|--org-dir)
            IS_ORG_ENABLED=1
            if [ -n "$2" ]; then
                ORG_DIR="$2"
            fi
            shift 2
            ;;
        -h|--help)
            printHelpMessage
            exit 0
            ;;
        --)
            shift
            break
            ;;
        *)
            printHelpMessage
            exit 1
            ;;
    esac
done

if [ "$#" -ne 1 ] || [ ! -d "$1" ] \
    || [ ! -f "$1/url.sh" ]; then
    echo "Error! No valid component directory has been provided."
    exit 1
fi

if [ -z "${SRC_DIR}" ]; then
    SRC_DIR="$1/${DEFAULT_SRC}"
fi
if [ -z "${ORG_DIR}" ]; then
    ORG_DIR="$1/${DEFAULT_ORG}"
fi
URL_SCRIPT_PATH="$1/url.sh"
if [ "${URL_SCRIPT_PATH:0:2}" != "./" ] \
    && [ "${URL_SCRIPT_PATH:0:1}" != "/" ]; then
    URL_SCRIPT_PATH="./${URL_SCRIPT_PATH}"
fi

if ! isParentDirExits "${SRC_DIR}"; then
    echo "Error! Specified source diretory is invalid."
    exit 1
fi
if ! isParentDirExits "${ORG_DIR}"; then
    echo "Error! Specified backup diretory is invalid."
    exit 1
fi

echo

echo "Retrieving tarball URL.."
SRC_TARBALL_URL="$(${URL_SCRIPT_PATH})"
echo "Found tarball at ${SRC_TARBALL_URL}"

echo "Downloading source tarball.."
SRC_TARBALL_PATH="$(mktemp)"
trap 'rm "${SRC_TARBALL_PATH}"' EXIT
wget "${SRC_TARBALL_URL}" -O "${SRC_TARBALL_PATH}"

echo "Extracting sourcess from tarball.."
recreateDirectory "${SRC_DIR}"
tar -xf "${SRC_TARBALL_PATH}" -C "${SRC_DIR}" --strip-components=1
rm "${SRC_TARBALL_PATH}"
trap - EXIT

if [ "${IS_ORG_ENABLED}" -eq 1 ]; then
    echo "Copying sourcess to backup location.."
    recreateDirectory "${ORG_DIR}"
    cp -r "${SRC_DIR}/." "${ORG_DIR}/"
fi

echo "Done!"
