#!/bin/bash
DEFAULT_SRC="src"
DEFAULT_ORG="src-org"
DEFAULT_PATCH="patch"
echoWithoutIndent() {
    local MESSAGE="$1"; shift
    local AWK_SRC='{ 
        match($0, /^[[:space:]]*/)
        if (length($0) > 0) print RLENGTH 
    }'
    local INDENT_SIZE="$( \
        echo "${MESSAGE}" \
        | awk "${AWK_SRC}" \
        | sort -n | head -n 1)"
    echo "${MESSAGE}" | sed -r "s|^.{${INDENT_SIZE}}||"
}
recreateDirectory() {
    local DIR_PATH="$1"; shift
    
    if [ -d "${DIR_PATH}" ]; then
        rm -rf "${DIR_PATH}"
    fi
    mkdir "${DIR_PATH}"
}
isParentDirExits() {
    local IN_PATH="$1"; shift
    local PARENT_DIR="$(dirname "${IN_PATH}")"
    [ -d "${PARENT_DIR}" ] || return 1
    return 0
}
silentCd() {
    local DIR_PATH="$1"; shift
    cd "${DIR_PATH}" > /dev/null 2>&1
}
