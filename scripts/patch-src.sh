#!/bin/bash
set -e
SCRIPT_DIR="$(dirname "${BASH_SOURCE}")"
. "${SCRIPT_DIR}/common.sh"
printHelpMessage() {
    echoWithoutIndent "
    Patches the source code

    Syntax: $0 [options] <component_dir>

    Options:
    -s, --src-dir <src_dir>         Specifies custom source code directory
    -p, --patch-dir <patch_dir>     Specifies custom patch directory                           
    -h, --help                      Print this message
    "
}
removeSuffix() {
    local FILE_PATH="$1"; shift

    if [[ "${FILE_PATH}" == *.patch. ]]; then
        echo "${FILE_PATH%.}"
        return
    fi
    echo "${FILE_PATH%.patch}"
}

echo "Source patcher script v1.0"

SRC_DIR=""
PATCH_DIR=""

PARSED_ARGS=$(LANG=C getopt --name "$0" --options="s:p:h" \
    --longoptions="src-dir:,patch-dir:,help" -- "$@") || exit 1
eval set -- "${PARSED_ARGS}"
while true; do
    case "$1" in
        -s|--src-dir)
            SRC_DIR="$2"
            shift 2
            ;;
        -p|--patch-dir)
            PATCH_DIR="$2"
            shift 2
            ;;
        -h|--help)
            printHelpMessage
            exit 0
            ;;
        --)
            shift
            break
            ;;
        *)
            printHelpMessage
            exit 1
            ;;
    esac
done

if [ "$#" -ne 1 ] || [ ! -d "$1" ]; then
    echo "Error! No valid component directory has been provided."
    exit 1
fi

if [ -z "${SRC_DIR}" ]; then
    SRC_DIR="$1/${DEFAULT_SRC}"
fi
if [ -z "${PATCH_DIR}" ]; then
    PATCH_DIR="$1/${DEFAULT_PATCH}"
fi

if [ ! -d "${SRC_DIR}" ]; then
    echo "Source directory does not exists"
    exit 1
fi
if [ ! -d "${PATCH_DIR}" ]; then
    echo "Patch directory does not exists"
    exit 1
fi

echo

PATCH_FILES_DIR="${PATCH_DIR}/files"
PATCH_DELETED_FILE="${PATCH_DIR}/deleted.txt"

echo "Patching source files.."

while IFS= read -r -d '' CURR_PATCH_FILE_REL_PATH; do
    CURR_SRC_FILE_REL_PATH="$(removeSuffix "${CURR_PATCH_FILE_REL_PATH}")"

    CURR_SRC_FILE="${SRC_DIR}/${CURR_SRC_FILE_REL_PATH}"
    CURR_PATCH_FILE="${PATCH_FILES_DIR}/${CURR_PATCH_FILE_REL_PATH}"

    mkdir -p "$(dirname "${CURR_SRC_FILE}")"
    if [[ "${CURR_PATCH_FILE}" == *.patch ]]; then
        echo "Patching ${CURR_SRC_FILE_REL_PATH}.."

        patch "${CURR_SRC_FILE}" "${CURR_PATCH_FILE}" > /dev/null
    else
        echo "Copying ${CURR_SRC_FILE_REL_PATH}.."

        cp "${CURR_PATCH_FILE}" "${CURR_SRC_FILE}"
    fi
done < <(
    {
        silentCd "${PATCH_FILES_DIR}"
        find . -type f -print0
        silentCd -
    } | sed -z 's|^\./||'
)

if [ -f "${PATCH_DELETED_FILE}" ]; then
    while IFS= read -r CURR_FILE_REL_PATH \
        || [ -n "$CURR_FILE_REL_PATH" ]; do
        CURR_SRC_FILE="${SRC_DIR}/${CURR_FILE_REL_PATH}"
        
        echo "Deleting ${CURR_FILE_REL_PATH}.."
        rm "${CURR_SRC_FILE}"
    done < "${PATCH_DELETED_FILE}"
fi

echo "Done!"
