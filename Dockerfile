FROM debian:bullseye-slim AS builder
RUN export DEBIAN_FRONTEND=noninteractive \
    && apt-get update \
    && apt-get install --yes --no-install-recommends \
        bash wget ca-certificates gawk file \
        patch diffutils gzip bzip2 xz-utils unzip \
        git build-essential autoconf automake \
        meson ninja-build flex bison gettext \
        texinfo help2man libncurses-dev libtool-bin \
        python3 python3-dev python-is-python3 \
        locales chrpath cpio diffstat \
        python3-distutils lz4 zstd makeself \
    && rm -rf /var/lib/apt/lists/* \
    && echo 'en_US.UTF-8 UTF-8' >> /etc/locale.gen && locale-gen
WORKDIR /scripts
COPY scripts ./
ENV QEMU_SRC_DIR=/tmp/qemu/component/src
WORKDIR /tmp/qemu/component
COPY qemu/component ./
RUN /scripts/get-src.sh . \
    && /scripts/patch-src.sh .
WORKDIR /tmp/sdk/component
COPY zephyr/sdk/component ./
RUN /scripts/get-src.sh . \
    && /scripts/patch-src.sh .
WORKDIR /tmp/sdk/scripts
COPY zephyr/sdk/scripts/build-deps.sh ./
RUN ./build-deps.sh
COPY zephyr/sdk/scripts/build-sdk.sh ./
RUN ./build-sdk.sh
COPY zephyr/sdk/scripts/build-setup.sh ./
RUN ./build-setup.sh
WORKDIR /out
RUN mv -t ./ /tmp/sdk/component/out/*.run

FROM debian:bullseye-slim
RUN export DEBIAN_FRONTEND=noninteractive \
    && echo 'deb http://deb.debian.org/debian bullseye-backports main' \
        > /etc/apt/sources.list.d/backports.list \
    && apt-get update \
    && apt-get install --yes --no-install-recommends \
        bash wget ca-certificates gawk file \
        patch diffutils gzip bzip2 xz-utils \
        git build-essential gcc-multilib g++-multilib \
        ninja-build device-tree-compiler \
        python3 python3-dev python3-pip \
        python3-setuptools python3-tk python3-wheel \
        ccache gperf dfu-util libsdl2-dev tmux \
    && apt-get install --yes --no-install-recommends \
        --target-release bullseye-backports cmake \
    && rm -rf /var/lib/apt/lists/*
WORKDIR /scripts
COPY scripts ./
ENV ZEPHYR_SDK_INSTALL_DIR="/sdk"
WORKDIR /tmp/sdk/component/out
COPY --from=builder /out/zephyr-sdk*.run ./
WORKDIR /tmp/sdk/scripts
COPY zephyr/sdk/scripts/setup-sdk.sh ./
RUN ./setup-sdk.sh
WORKDIR /work/app
COPY app ./
ENV PATH="/root/.local/bin:${PATH}"
RUN pip3 install --user -U west \
    && west init -l . \
    && west update \
    && west zephyr-export \
    && pip3 install --user -r ../zephyr/scripts/requirements.txt
WORKDIR /tmp/os/component
COPY zephyr/os/component ./
RUN /scripts/patch-src.sh --src-dir /work/zephyr .
WORKDIR /work/app
ENTRYPOINT ["/bin/bash"]
