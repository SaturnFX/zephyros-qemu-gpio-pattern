# ZephyrOS QEMU GPIO Pattern
This project is an example solution how to achieve peripheral devices simulation during emulation of a ZephyrOS program. For simplicity, simulation is enabled only for the ST Nucleo F103RB board. Also,  the general case for peripherals simulation has been limited to simple GPIO pins manipulation. The solution is in the form of a shell script that builds a binary of sample ZephyrOS app and runs the emulator and the simulation shell in 2 separate terminal panels. The ZephyrOS environment used by this script is fully containerized and all the dependencies are set up automatically.

## Requirements
You can test this project on any Linux-based system with the following packages installed: 
* Bash
* Docker Engine

## How to use?
![A demo GIF](demo.gif)

To run the simulation demo, you have to run a script located in this repository as follows:
```
$ ./run-demo.sh
```
After preparing a container image and building the sample app, the emulator will run the program on the left panel and the simulation shell will open on the right panel. You can verify the simulation by writing the value of a PC2 pin - using `write PC2=value` command and checking the value of a PC3 pin - using `read PC3` command. At the same time, the app will constantly change the PC3 value and print the PC2 value. For more information on how to use the simulation shell, use the `help` command.
