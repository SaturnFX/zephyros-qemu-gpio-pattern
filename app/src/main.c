/*
 * Copyright (c) 2022 Michał Prochera
 * Copyright (c) 2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <stdio.h>
#include <string.h>

#include <zephyr.h>
#include <sys/printk.h>
#include <devicetree.h>
#include <drivers/gpio.h>

#define ZEPHYR_USER_NODE DT_PATH(zephyr_user)

#define GPIO_NAME_SIZE 5

#define SLEEP_TIME_S 10

static const struct gpio_dt_spec in_dt =
    GPIO_DT_SPEC_GET(ZEPHYR_USER_NODE, in_gpios);

static const struct gpio_dt_spec out_dt =
    GPIO_DT_SPEC_GET(ZEPHYR_USER_NODE, out_gpios);

static inline bool init_gpio_pins()
{
    if (gpio_pin_configure_dt(&in_dt, GPIO_INPUT)) {
        return false;
    }

    if (gpio_pin_configure_dt(&out_dt, GPIO_OUTPUT_INACTIVE)) {
        return false;
    }

    return true;
}

static inline void get_gpio_name(
    const struct gpio_dt_spec *gpio_dt, char *out)
{
    const char *port_name = gpio_dt->port->name;

    out[0] = 'P';
    out[1] = port_name[strlen(port_name) - 1];
    snprintf(&out[2], GPIO_NAME_SIZE - 2, "%u", gpio_dt->pin);
}

void main(void)
{
    int index = 0;
    int in_val = 0;
    int out_val = 0;
    char in_name[GPIO_NAME_SIZE];
    char out_name[GPIO_NAME_SIZE];

    printk("GPIO sample (on %s)\n\n", CONFIG_BOARD);

    if (!init_gpio_pins()) {
        printk("Error configuring GPIO pins! Exiting..\n");
        return;
    }

    get_gpio_name(&in_dt, in_name);
    get_gpio_name(&out_dt, out_name);

    while (true) {
        k_msleep(SLEEP_TIME_S * 1000);

        in_val = gpio_pin_get_dt(&in_dt);
        gpio_pin_set_dt(&out_dt, out_val);

        printk("**** Iteration %d ****\n", index);
        printk("Read %s value    = %d\n",
            in_name, in_val);
        printk("Written %s value = %d\n\n",
            out_name, out_val);

        out_val = !out_val;
        ++index;
    }
}
